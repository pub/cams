//
// make_index.js
//    make shopcam activity index
//    Neil Gershenfeld
//    10/5/17
//
// requires
//
const fs = require('fs')
const exec = require('child_process').exec
const execSync = require('child_process').execSync
//
// make index
//
execSync("echo '<html><body>' > index.html")
for (var month = 1; month <= 12; ++month) {
   for (var day = 1; day <= 31; ++day) {
      execSync("echo '<a href=index/"+
         ('0'+month).slice(-2)+"-"+('0'+day).slice(-2)+".html>"+
         ('0'+month).slice(-2)+"-"+('0'+day).slice(-2)+
         "</a>' >> index.html")
      execSync("echo '<html><body>' > index/newindex.html")
      execSync("mv index/newindex.html index/"+
         ('0'+month).slice(-2)+"-"+('0'+day).slice(-2)+".html")
      }
   }

