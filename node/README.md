<pre>
shopcam server, prior node version

to clone: git clone https://gitlab.cba.mit.edu/pub/cams.git

to reinitialize: ./init

to run: node cams.js

uses ffmpeg for screen capture

uses convert to make thumbnails

uses compare to measure activity

can use uvcdynctrl -f to list resolutions

can use sudo guvcview to adjust camera

can use http-server-with-auth to serve:  
http-server-with-auth -p port --username username --password password  
http-server-with-auth -p port --username username --password password --ssl --cert cert.pem --key key.pem  
index is at http://this_address:this_port  
last image is at http://this_address:this_port/oldimage.jpg  

can use Ubuntu MATE on Raspberry Pi 3:  
sudo apt-get install nodejs-legacy npm ffmpeg imagemagick guvcview  
sudo npm install -g http-server-with-auth  
System -> Administration -> Time and Date -> Keep synchronized with internet servers  

can use Logitech C920 HD Webcam

can start cams script on boot with cams.service:  
edit cams script with port, username, and password
sudo cp cams.service /lib/systemd/system  
sudo systemctl daemon-reload  
sudo systemctl enable cams.service  
sudo systemctl start cams.service  
journalctl -u cams.service
