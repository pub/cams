<pre>
shopcam server

can use Ubuntu 18.04 with an HD Webcam
   sudo apt-get updat
   sudo apt-get dist-upgrade
   sudo apt-get install git ristretto tightvncserver ntp openssh-server motion
   sudo systemctl enable ssh
   System -> Administration -> Time and Date -> Keep synchronized with internet servers
   sudo update-manager -> turn off automatic updates

clone repos:
   git clone https://gitlab.cba.mit.edu/pub/cams.git
   git clone https://github.com/novnc/noVNC.git

set up motion
   mkdir ~/.motion
   cp ~/cams/motion/motion.conf ~/.motion
   mousepad ~/.motion/motion.conf

set VNC password:
   tightvncpasswd

set up VNC:
   cp ~/cams/motion/xstartup ~/.vnc/
   chmod +x ~/.vnc/xstartup
   can use https certificates

install VNC service for remote picture viewing:
   cd ~/cams/motion
   sudo cp tightvncserver.service /lib/systemd/system
   sudo systemctl daemon-reload
   sudo systemctl enable tightvncserver.service
   sudo systemctl start tightvncserver.service
   journalctl -u tightvncserver.service

reboot

connect to VNC:
   http://your_ip_address:6789/vnc.html
   view and set configuration at http://127.0.0.1:8081

connect to stream:
   http://your_ip_address:8080
