<pre>
shopcam server

can use Ubuntu MATE on a Raspberry Pi 3 with a Logitech C920 HD Webcam
   sudo apt-get update
   sudo apt-get dist-upgrade
   sudo apt-get install gedit chromium-browser git npm nodejs ristretto tightvncserver xtightvncviewer openssh-server ntp
   sudo systemctl enable ssh
   System -> Administration -> Time and Date -> Keep synchronized with internet servers
   sudo update-manager -> turn off automatic updates

install local web server:
   sudo npm install -g http-server-with-auth  

clone repos:
   git clone https://gitlab.cba.mit.edu/pub/cams.git
   git clone https://gitlab.cba.mit.edu/pub/mods.git
   git clone https://github.com/novnc/noVNC.git

install VNC service:
   cd ~/cams/mods
   sudo cp tightvncserver.service /lib/systemd/system
   sudo systemctl daemon-reload
   sudo systemctl enable tightvncserver.service
   sudo systemctl start tightvncserver.service
   journalctl -u tightvncserver.service

set VNC password:
   tightvncpasswd

configure VNC:
   cp ~/cams/mods/xstartup /home/fab/.vnc/
   gedit /home/fab/.vnc/xstartup
   set the local Web server username and password
   can use https certificates

configure noVNC:
   cd ~/noVNC
   ./utils/launch.sh --listen 6789 --vnc 127.0.0.1:5901 &

configure network

reboot

connect to VNC:
   http://your_ip_address:6789/vnc.html

once connected:
   approve browser request to use camera and download multiple files
   can use ristretto to view images
   can use guvcview to adjust camera
   to make a persistent change to the threshold,
      edit the motion detect module initialization,
      save it as a local program,
      and change the path in xstartup
