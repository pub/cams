<pre>
site:
   https://www.kerberos.io/
documentation:
   https://doc.kerberos.io/
project:
   https://github.com/kerberos-io
download:
   https://github.com/kerberos-io/kios/releases
configure:
   edit static_ip.conf
      set the address
   boot
   ssh to root@the_static_address
   edit /data/etc/os.conf
      set os_debug to true
   reboot
   ssh to root@the_static_address
      use passwd to change the root password
   view http://the_static_address
      set name and password
      in Configuration switch to Advanced
         set the Timezone
         set the Capture source
            set the resolution, delay(2000ms?) and angle
         set the Stream password
         Update
      in Configuration switch to Basic
         in Surveillance
            set region
            set sensitivity
            set outputs
         Update
